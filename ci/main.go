package main

import (
    "context"
    "fmt"
    "os"

    "dagger.io/dagger"
)

func main() {
    // Create dagger client
    ctx := context.Background()
    client, err := dagger.Connect(ctx, dagger.WithLogOutput(os.Stdout))
    if err != nil {
        panic(err)
    }
    defer client.Close()

    project := client.Host().Directory(".")

    // Build our app
    builder := client.Container().
        From("golang:latest").
        WithMountedDirectory("/src", project).
        WithWorkdir("/src").
        WithEnvVariable("CGO_ENABLED", "0").
        WithExec([]string{"go", "build", "-o", "myapp"})

    // Publish binary on Alpine base
    prodImage := client.Container().
        From("alpine").
        WithFile("/bin/myapp", builder.File("/src/myapp")).
        WithEntrypoint([]string{"/bin/myapp"})

    addr, err := prodImage.Publish(ctx, "gcr.io/vikram-experiments/myapp")
    if err != nil {
        panic(err)
    }

    fmt.Println(addr)
    /*
import { ServicesClient } from "@google-cloud/run";

const GCR_SERVICE_URL = 'projects/vikram-experiments/locations/us-central1/services/myapp'
const GCR_PUBLISH_ADDRESS = 'gcr.io/  // initialize Google Cloud Run client
  const gcrClient = new ServicesClient();

  // define service request
  const gcrServiceUpdateRequest = {
    service: {
      name: GCR_SERVICE_URL,
      template: {
        containers: [
          {
            image: gcrContainerPublishResponse,
            ports: [
              {
                name: "http1",
                containerPort: 3000
              }
            ]
          }
        ],
      },
     }
  };

  // update service
  const [gcrServiceUpdateOperation] = await gcrClient.updateService(gcrServiceUpdateRequest);
  const [gcrServiceUpdateResponse] = await gcrServiceUpdateOperation.promise();

  // print ref
  console.log(`Deployment for image ${gcrContainerPublishResponse} now available at ${gcrServiceUpdateResponse.uri}`)/myapp'
  // initialize Google Cloud Run client
  const gcrClient = new ServicesClient();

  // define service request
  const gcrServiceUpdateRequest = {
    service: {
      name: GCR_SERVICE_URL,
      template: {
        containers: [
          {
            image: gcrContainerPublishResponse,
            ports: [
              {
                name: "http1",
                containerPort: 3000
              }
            ]
          }
        ],
      },
     }
  };

  // update service
  const [gcrServiceUpdateOperation] = await gcrClient.updateService(gcrServiceUpdateRequest);
  const [gcrServiceUpdateResponse] = await gcrServiceUpdateOperation.promise();

  // print ref
  console.log(`Deployment for image ${gcrContainerPublishResponse} now available at ${gcrServiceUpdateResponse.uri}`)
    */
}
