package main

import (
    "context"
    "fmt"

		run "cloud.google.com/go/run/apiv2"
		runpb "cloud.google.com/go/run/apiv2/runpb"

)

func main() {

		ctx := context.Background()
		// This snippet has been automatically generated and should be regarded as a code template only.
		// It will require modifications to work:
		// - It may require correct/in-range values for request initialization.
		// - It may require specifying regional endpoints when creating the service client as shown in:
		//   https://pkg.go.dev/cloud.google.com/go#hdr-Client_Options
		c, err := run.NewServicesClient(ctx)
		if err != nil {
			fmt.Println(err)
						// TODO: Handle error.
		}
		defer c.Close()

		req := &runpb.GetServiceRequest{
			Name: "projects/vikram-experiments/locations/us-central1/services/myapp",
						// TODO: Fill request struct fields.
						// See https://pkg.go.dev/cloud.google.com/go/run/apiv2/runpb#GetServiceRequest.
		}
		resp, err := c.GetService(ctx, req)
		if err != nil {
			fmt.Println("error2")
						// TODO: Handle error.
		}
		// TODO: Use resp.
		fmt.Println(resp)

}


/*
import { ServicesClient } from "@google-cloud/run";

const GCR_SERVICE_URL = 'projects/vikram-experiments/locations/us-central1/services/myapp'
const GCR_PUBLISH_ADDRESS = 'gcr.io/  // initialize Google Cloud Run client
  const gcrClient = new ServicesClient();

  // define service request
  const gcrServiceUpdateRequest = {
    service: {
      name: GCR_SERVICE_URL,
      template: {
        containers: [
          {
            image: gcrContainerPublishResponse,
            ports: [
              {
                name: "http1",
                containerPort: 3000
              }
            ]
          }
        ],
      },
     }
  };

  // update service
  const [gcrServiceUpdateOperation] = await gcrClient.updateService(gcrServiceUpdateRequest);
  const [gcrServiceUpdateResponse] = await gcrServiceUpdateOperation.promise();

  // print ref
  console.log(`Deployment for image ${gcrContainerPublishResponse} now available at ${gcrServiceUpdateResponse.uri}`)/myapp'
  // initialize Google Cloud Run client
  const gcrClient = new ServicesClient();

  // define service request
  const gcrServiceUpdateRequest = {
    service: {
      name: GCR_SERVICE_URL,
      template: {
        containers: [
          {
            image: gcrContainerPublishResponse,
            ports: [
              {
                name: "http1",
                containerPort: 3000
              }
            ]
          }
        ],
      },
     }
  };

  // update service
  const [gcrServiceUpdateOperation] = await gcrClient.updateService(gcrServiceUpdateRequest);
  const [gcrServiceUpdateResponse] = await gcrServiceUpdateOperation.promise();

  // print ref
  console.log(`Deployment for image ${gcrContainerPublishResponse} now available at ${gcrServiceUpdateResponse.uri}`)
    */
